//
//  StringExtend.swift
//  RSSGomoku
//
//  Created by István Stefánovics on 18/04/16.
//  Copyright © 2016 István Stefánovics. All rights reserved.
//

import Foundation

extension String {
    var ns: NSString {
        return self as NSString
    }
    var pathExtension: String? {
        return ns.pathExtension
    }
    var lastPathComponent: String? {
        return ns.lastPathComponent
    }
}
