//
//  InterfaceController.swift
//  RSSGomoku WatchKit Extension
//
//  Created by István Stefánovics on 13/04/16.
//  Copyright © 2016 István Stefánovics. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    //first line
    @IBOutlet var button_11: WKInterfaceButton!
    @IBOutlet var button_12: WKInterfaceButton!
    @IBOutlet var button_13: WKInterfaceButton!
    
    //second line
    @IBOutlet var button_21: WKInterfaceButton!
    @IBOutlet var button_22: WKInterfaceButton!
    @IBOutlet var button_23: WKInterfaceButton!
    
    //third line
    @IBOutlet var button_31: WKInterfaceButton!
    @IBOutlet var button_32: WKInterfaceButton!
    @IBOutlet var button_33: WKInterfaceButton!
    
    @IBOutlet var statusLabel: WKInterfaceLabel!
    
    var gameManager: RssGameManager = RssGameManager()
    
    override init()
    {
        super.init()
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        resetButtons()
        updateStatusLabel()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    //MARK: - Private
    
    
    
    func buttonPressed(button: WKInterfaceButton, row: NSInteger, column: NSInteger) {
        
        if(!gameManager.isResetNeeded()) {
            
            let markResult = gameManager.markPositionAt(row, column: column)
            
            if(markResult.mark != Mark.None){
                if markResult.mark == Mark.Player1 {
                    button.setBackgroundImageNamed("x_icon")
                }
                else
                {
                    button.setBackgroundImageNamed("o_icon")
                }
            }
            
            if markResult.winning == true {
                
                
                statusLabel.setText("Player: \(gameManager.currentPlayer() == Mark.Player1 ? "1": "2") has WON!")
                statusLabel.setTextColor(UIColor(red: 0.502, green: 1.0, blue: 0.0, alpha: 1.0))
            }
            else if markResult.winning == true && gameManager.isResetNeeded() {
                statusLabel.setText("STATUS: DRAW!")
                statusLabel.setTextColor(UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0))
            }
            else
            {
                updateStatusLabel()
            }
        }
    }
    
    func resetButtons() {
        let buttons = [button_11, button_12, button_13, button_21, button_22, button_23, button_31, button_32, button_33]
        
        for button: WKInterfaceButton in buttons {
            button.setBackgroundImageNamed("button_bg")
        }
    }
    
    func updateStatusLabel()  {
        statusLabel.setText("PLayer: \(gameManager.currentPlayer().rawValue)")
    }
    
    // MARK: - IBActions
    
    //first line
    @IBAction func button_11_tapped()
    {
        buttonPressed(button_11, row: 0, column: 0)
    }
    
    @IBAction func button_12_tapped()
    {
        buttonPressed(button_12, row: 0, column: 1)
    }
    
    @IBAction func button_13_tapped()
    {
        buttonPressed(button_13, row: 0, column: 2)
    }
    
    //second line
    @IBAction func button_21_tapped()
    {
        buttonPressed(button_21, row: 1, column: 0)
    }
    
    @IBAction func button_22_tapped()
    {
        buttonPressed(button_22, row: 1, column: 1)
    }
    
    @IBAction func button_23_tapped()
    {
        buttonPressed(button_23, row: 1, column: 2)
    }
    
    //third line
    @IBAction func button_31_tapped()
    {
        buttonPressed(button_31, row: 2, column: 0)
    }
    
    @IBAction func button_32_tappped()
    {
        buttonPressed(button_32, row: 2, column: 1)
    }
    
    @IBAction func button_33_tapped()
    {
        buttonPressed(button_33, row: 2, column: 2)
    }
    
    @IBAction func resetButtonTapped() {
        gameManager.reset()
        resetButtons()
        statusLabel.setTextColor(UIColor(red: 0.946, green: 0.9508, blue: 0.9507, alpha: 1.0))
        updateStatusLabel()
    }
}
