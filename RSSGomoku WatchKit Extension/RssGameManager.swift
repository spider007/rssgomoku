//
//  RssGameManager.swift
//  RSSGomoku
//
//  Created by István Stefánovics on 24/04/16.
//  Copyright © 2016 István Stefánovics. All rights reserved.
//

import Foundation

class RssGameManager : NSObject
{
    private var game: RSSGomukuGame = RSSGomukuGame.init(size: 3)
    
    override init()
    {
        super.init()
    }
    
    func markPositionAt(row: NSInteger, column: NSInteger) -> (mark: Mark,winning: Bool)  {
        return game.markPosition(game.currentPlayer, row: row, column: column)
    }
    
    func currentPlayer() -> Mark{
        return game.currentPlayer
    }
    
    func reset() {
        game.resetGame(3)
    }
    
    func isResetNeeded() -> Bool {
        return game.resetNeeded
    }
    
}