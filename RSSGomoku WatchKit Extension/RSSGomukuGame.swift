//
//  RSSGomukuGame.swift
//  RSSGomoku
//
//  Created by István Stefánovics on 18/04/16.
//  Copyright © 2016 István Stefánovics. All rights reserved.
//

import UIKit

let mark_needed = 3

enum Mark : String
{
    case None = " "
    case Player1 = "X"
    case Player2 = "O"
}

struct direction
{
    var row: NSInteger
    var column: NSInteger
}

class RSSGomukuGame: NSObject
{
    private var table: [[Mark]]
    internal var currentPlayer: Mark = .None
    internal var markCount : NSInteger = 0
    private var tableSize:NSInteger
    private var maxMarkCount: NSInteger
    internal var resetNeeded: Bool = false
    
    /**
     -1/-1 | -1/+0 | +1/-1
     +0/-1 |  X/X  | +0/+1
     -1/+1 | +1/+0 | +1/+1
     */
    private let directions = [[direction(row: -1, column: -1), direction(row: 1, column: 1)],
                              [direction(row: 1, column: -1), direction(row: -1, column: 1)],
                              [direction(row: -1, column: 0), direction(row: 1, column: 0)],
                              [direction(row: 0, column: -1), direction(row: 0, column: 1)]]
    
    init(size: NSInteger) {
        tableSize = size
        maxMarkCount = size * size
        table = Array(count: size, repeatedValue: Array(count: size, repeatedValue: Mark.None))
        
        super.init()
        
        self.resetGame(size)
    }
    
    func markPosition(player: Mark, row: NSInteger, column: NSInteger) -> (mark: Mark,winning: Bool) {
        var mark = Mark.None
        var winning = false
        
        if player == currentPlayer && table[row][column] == Mark.None
        {
            table[row][column] = player
            markCount += 1
            
            if markCount == self.maxMarkCount
            {
                resetNeeded = true
            }
            
            mark = player
            
            if(markCount > 3){
                winning = checkForWinning(row, column: column)
            } else {
                setNextPlayer()

            }
        }
        
        return (mark: mark,winning: winning)
    }
    
    func checkForWinning(row: NSInteger, column: NSInteger) -> Bool {
        var result = false
        
        for origin in self.directions
        {
            var resultInRow = 1
            
            resultInRow += checkInDirection(origin[0], row: row, column: column)
            resultInRow += checkInDirection(origin[1], row: row, column: column)
            
            if(resultInRow >= mark_needed)
            {
                result = true
                break
            }
        }
        
        return result
    }
    
    private func checkInDirection(checkDirection: direction, row: NSInteger, column: NSInteger) -> NSInteger {
        var result = 0
        var checkRow = row + checkDirection.row
        var checkColumn = column + checkDirection.row
        
        while self.table[row][column] == self.getMark(checkColumn, row: checkRow)
        {
            result += 1
            checkRow += checkDirection.row
            checkColumn += checkDirection.row
        }
        
        return result
    }
    
    private func getMark(column: NSInteger, row: NSInteger) -> Mark {
        if column <  0 || row < 0 || column > tableSize - 1 || row > tableSize - 1
        {
            return Mark.None
        }
        
        return table[column][row]
    }
    
    private func setNextPlayer() {
        currentPlayer = (currentPlayer == Mark.Player1) ? Mark.Player2 : Mark.Player1
    }
    
    func resetGame(size: NSInteger) {
        currentPlayer = .Player1
        markCount = 0
        resetNeeded = false
        
        table = Array(count: size, repeatedValue: Array(count: size, repeatedValue: Mark.None))
    }
}
