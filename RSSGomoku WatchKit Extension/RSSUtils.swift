//
//  RSSUtils.swift
//  RSSGomoku
//
//  Created by István Stefánovics on 18/04/16.
//  Copyright © 2016 István Stefánovics. All rights reserved.
//

import UIKit

let dateFormatter = NSDateFormatter()


func RSSLog<T>(object: T, filename: String = #file, line: Int = #line, funcname: String = #function) {
    dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss:SSS"
    let process = NSProcessInfo.processInfo()
    let threadId = pthread_mach_thread_np(pthread_self())
    print("\(dateFormatter.stringFromDate(NSDate())) \(process.processName))[\(process.processIdentifier):\(threadId)] \(filename.lastPathComponent)(\(line)) \(funcname):\r\t\(object)\n")
}
